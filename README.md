# haste.py upload script
This is a small python script used to upload text to hastebin.com.

## Installation
* Download the repository

```shell
git clone https://gitlab.com/rehtlaw/haste.py.git
```

* Install the required package

```shell
sudo pip install -r requirements.txt
```

* Copy the `haste` file somewhere in your path (e.g. /usr/bin/)

```shell
sudo cp haste /usr/bin/haste
```

## Usage
Just pipe the output of any file (using cat) into this script.
```shell
cat myfile.txt | haste
```
It will then provide you with a URL to the hastebin. If there is something wrong
with the upload, it will print out the errormessage.
